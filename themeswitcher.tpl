<div id="themeswitcher">
    <form method="get">
        <select name="_theme">
            {foreach from=$themes item=theme}
                <option value="{$theme->name}" {if ($theme->name == $active)}selected="selected" {/if}>{$theme->name}</option>
            {/foreach}
        </select>
        <button type="submit">{l s="Change" mod="themeswitcher"}</button>
    </form>
</div>