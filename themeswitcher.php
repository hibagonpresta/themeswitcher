<?php

// Security
if (!defined('_PS_VERSION_'))
    exit;

// Checking compatibility with older PrestaShop and fixing it
if (!defined('_MYSQL_ENGINE_'))
    define('_MYSQL_ENGINE_', 'MyISAM');

// Loading Models
require_once(_PS_MODULE_DIR_ . 'example/models/ExampleData.php');

class ThemeSwitcher extends Module
{
    public function __construct()
    {
        $this->author = 'Błażej Adamczyk';
        $this->name = 'themeswitcher';
        $this->tab = 'others';

        $this->version = '1.1.1';

        $this->ps_versions_compliancy['min'] = '1.5';
        $this->ps_versions_compliancy['max'] = '1.6';

        $this->need_instance = 0;

        $this->dependencies = array();

        $this->limited_countries = array();

        parent::__construct();

        $this->displayName = $this->l('Theme Switcher');
        $this->description = $this->l('Module strictly for frontend & themes developers. Allows you to quickly change theme');

        $this->confirmUninstall = $this->l('Are you sure you want to delete this module ?');

        if ($this->active)
            $this->warning = $this->l('You have to configure your module');
    }

    public function install()
    {
        return parent::install() &&
        $this->registerHook('header') &&
        $this->registerHook('displayFooter');
    }

    public function uninstall()
    {
        // Uninstall Module
        if (!parent::uninstall()
            || !$this->unregisterHook('displayFooter')
            || !$this->unregisterHook('header')
        )
            return false;


        return true;
    }

    public function hookHeader() {
        if(isset($_GET['_theme'])) {
            $title = pSQL($_GET['_theme']);
            $theme = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT `id_theme` FROM `'._DB_PREFIX_.'theme` WHERE `name` = "'.$title.'"');
            $success = Db::getInstance(_PS_USE_SQL_SLAVE_)->execute('UPDATE  `'._DB_PREFIX_."shop` SET `id_theme` = '{$theme}' WHERE `"._DB_PREFIX_."shop`.`id_shop` = {$this->context->shop->id};");
            if ($success) {
                Tools::redirect('/');
            }
        }
        $this->context->controller->addJS($this->_path . 'themeswitcher.js');
        $this->context->controller->addCSS($this->_path . 'themeswitcher.css', 'all');
    }

    public function hookDisplayFooter()
    {
        $themes = Theme::getThemes();

        $active = $this->context->shop->getTheme();

        $this->context->smarty->assign('request_uri', Tools::safeOutput($_SERVER['REQUEST_URI']));
        $this->context->smarty->assign('path', $this->_path);
        $this->context->smarty->assign('themes', $themes);
        $this->context->smarty->assign('active', $active);


        return $this->display(__FILE__, 'themeswitcher.tpl');
    }
}
